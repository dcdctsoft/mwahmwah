INTRO
-----

MwahMwah game was inspired from the winning exclamation from the Author upon winning the game from few arcade games, especially the one that Author found to have 'pattern' in winning the jackpot.

The jackpot could be the ticket, cash, or any kind of prizes.

This particular game is derived from the previously known game as 'pearls' where the pearls being pushed and few of the winning token being pushed as part of the pearl 'pool'.

Once the player have 'shot' certain amount of pearls (say ... 42?), the player is presented with jackpot 'wheel of fortune' kind of mini game.

All the player need to do for winning the jackpot is hitting the stop button at the right position of the wheel.
